// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
  firebase: {
    apiKey: "AIzaSyCc90EGQ382V2mQ9V8sI0NMgMn5SuSCXZw",
    authDomain: "imcionic.firebaseapp.com",
    projectId: "imcionic",
    storageBucket: "imcionic.appspot.com",
    messagingSenderId: "271295982258",
    appId: "1:271295982258:web:5ef7d522af9bd38779f7ab",
    measurementId: "G-ZXF3H25RZ6"
  }
};

