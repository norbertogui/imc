import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private firestore: AngularFirestore) { }

  collectionName = 'Pessoas';

 

  create_pessoas(record) {
    return this.firestore.collection(this.collectionName).add(record);
  }

  read_pessoas() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }

  update_pessoas(recordID, record) {
    this.firestore.doc(this.collectionName + '/' + recordID).update(record);
  }

  delete_pessoas(record_id) {
    this.firestore.doc(this.collectionName + '/' + record_id).delete();
  }
}
