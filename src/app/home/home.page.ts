import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseService } from '../firebase.service';
import { ImcService } from './imc.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  IMCForm: FormGroup;
  valorImc;
  Imc = [];
  pessoasList = [];


  constructor(
    private firebaseService: FirebaseService ,
    private ImcService: ImcService,
    private formBuilder: FormBuilder,
    private router: Router) {

      this.IMCForm = this.formBuilder.group({
        Nome: [null],
        Peso: [null],
        Altura: [null],
        CalculoImc: [this.valorImc]
      
      })

      this.ImcService.read_IMC().subscribe(data => {
  
        this.Imc = data.map(e => {
          return {
            id: e.payload.doc.id,
            Nome: e.payload.doc.data()['Nome'],
            Peso: e.payload.doc.data()['Peso'],
            Altura: e.payload.doc.data()['Altura'],
            CalculoImc: e.payload.doc.data()['CalculoImc'],
          };
        })
        console.log(this.Imc);
  
      });

      this.firebaseService.read_pessoas().subscribe(data => {

        this.pessoasList = data.map(e => {
          return {
            id: e.payload.doc.id,
            Nome: e.payload.doc.data()['Nome'],
            Idade: e.payload.doc.data()['Idade'],
          };
        })
        console.log(this.pessoasList);
  
      });
    
  


   
  }


  onSubmit() {
  }

  cadastraPessoas() {
    this.router.navigate(['/pessoas']);
  }


  Calcular() {
   
    console.log(this.Imc)

    let peso = this.IMCForm.value.Peso
    let Altura = this.IMCForm.value.Altura
    let Imc = peso / (Altura * Altura);
    console.log(Imc);
    this.IMCForm.value.CalculoImc = Imc.toFixed(1);

      this.ImcService.create_IMC(this.IMCForm.value).then(resp => {
        console.log(resp)
        this.IMCForm.reset();
      })
        .catch(error => {
          console.log(error);
        });
        this.IMCForm.reset();

  }

  delete(item){
    this.ImcService.delete_IMC(item)
  }

}
