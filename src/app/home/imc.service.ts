import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ImcService {


  constructor(private firestore: AngularFirestore) { }

  collectionName = 'IMC';

 

  create_IMC(record) {
    return this.firestore.collection(this.collectionName).add(record);
  }

  read_IMC() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }

  update_IMC(recordID, record) {
    this.firestore.doc(this.collectionName + '/' + recordID).update(record);
  }

  delete_IMC(record_id) {
    this.firestore.doc(this.collectionName + '/' + record_id).delete();
  }
}
