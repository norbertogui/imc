import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseService } from '../firebase.service';

@Component({
  selector: 'app-pessoas',
  templateUrl: './pessoas.page.html',
  styleUrls: ['./pessoas.page.scss'],
})
export class PessoasPage implements OnInit {

  pessoasForm: FormGroup
  pessoasList = [];

  constructor( 
    private router: Router,
    private fbuilder: FormBuilder,
    private firebaseService: FirebaseService

  ) {
    this.pessoasForm = this.fbuilder.group({
      Nome: [null],
      Idade: [null]
    })
  }

  ngOnInit() {
    this.firebaseService.read_pessoas().subscribe(data => {

      this.pessoasList = data.map(e => {
        return {
          id: e.payload.doc.id,
          Nome: e.payload.doc.data()['Nome'],
          Idade: e.payload.doc.data()['Idade'],
        };
      })
      console.log(this.pessoasList);

    });
  }

  voltar() {
    this.router.navigate(['/home']);
  }


  CreateRecord() {
    console.log(this.pessoasForm.value);
    this.firebaseService.create_pessoas(this.pessoasForm.value).then(resp => {
      this.pessoasForm.reset();
    })
      .catch(error => {
        console.log(error);
      });
  }



  deletar(item) {
   console.log(this.pessoasList.indexOf(item))
    this.firebaseService.delete_pessoas(item);
  }






}
